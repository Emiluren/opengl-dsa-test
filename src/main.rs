use glfw::{Action, Context, Key};
use gl::types::*;
use std::ffi::CString;
use std::{mem, ptr, str};
use std::os::raw::{c_char, c_void};

fn compile_shader(src: &str, ty: GLenum) -> GLuint {
    let shader;
    unsafe {
        shader = gl::CreateShader(ty);

        let c_str = CString::new(src.as_bytes()).unwrap();
        gl::ShaderSource(shader, 1, &c_str.as_ptr(), ptr::null());
        gl::CompileShader(shader);

        let mut status = gl::FALSE as GLint;
        gl::GetShaderiv(shader, gl::COMPILE_STATUS, &mut status);

        if status != (gl::TRUE as GLint) {
            panic!("Shader compilation failed");
        }
    }
    shader
}

fn link_program(vs: GLuint, fs: GLuint) -> GLuint {
    unsafe {
        let program = gl::CreateProgram();
        gl::AttachShader(program, vs);
        gl::AttachShader(program, fs);
        gl::LinkProgram(program);

        let mut status = gl::FALSE as GLint;
        gl::GetProgramiv(program, gl::LINK_STATUS, &mut status);

        if status != (gl::TRUE as GLint) {
            panic!("Shader program linking failed");
        }
        program
    }
}

static VERTEX_POSITIONS: [GLfloat; 6] = [
    0.0, 0.5,
    0.5, -0.5,
    -0.5, -0.5,
];

static VERTEX_COLORS: [GLfloat; 9] = [
    1.0, 0.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 0.0, 1.0,
];

extern "system" fn debug_print(_source: GLenum, _msg_type: GLenum, _id: GLuint, _severity: GLenum, _length: GLsizei, message: *const c_char, _param: *mut c_void) {
    unsafe {
        let opengl_message = std::ffi::CStr::from_ptr(message);
        dbg!(opengl_message);
    }
}

fn main() {
    let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();

    glfw.window_hint(glfw::WindowHint::ContextVersion(4, 5));

    let (mut window, events) = glfw.create_window(300, 300, "Hello this is window", glfw::WindowMode::Windowed)
        .expect("Failed to create GLFW window.");

    window.set_key_polling(true);
    window.set_resizable(true);
    window.set_size_polling(true);
    window.make_current();

    gl::load_with(|s| window.get_proc_address(s) as *const _);

    unsafe {
        gl::Enable(gl::DEBUG_OUTPUT);
        gl::DebugMessageCallback(Some(debug_print), ptr::null());
    }

    let vs = compile_shader(include_str!("shader.vert"), gl::VERTEX_SHADER);
    let fs = compile_shader(include_str!("shader.frag"), gl::FRAGMENT_SHADER);
    let program = link_program(vs, fs);

    let mut vao = 0;
    let mut pos_vbo = 0;
    let mut col_vbo = 0;

    unsafe {
        gl::BindFragDataLocation(program, 0, CString::new("out_color").unwrap().as_ptr());

        gl::CreateVertexArrays(1, &mut vao);
        gl::CreateBuffers(1, &mut pos_vbo);
        gl::NamedBufferData(
            pos_vbo,
            (VERTEX_POSITIONS.len() * mem::size_of::<GLfloat>()) as GLsizeiptr,
            mem::transmute(&VERTEX_POSITIONS[0]),
            gl::STATIC_DRAW,
        );
        gl::CreateBuffers(1, &mut col_vbo);
        gl::NamedBufferData(
            col_vbo,
            (VERTEX_COLORS.len() * mem::size_of::<GLfloat>()) as GLsizeiptr,
            mem::transmute(&VERTEX_COLORS[0]),
            gl::STATIC_DRAW,
        );

        let pos_attr = gl::GetAttribLocation(program, CString::new("position").unwrap().as_ptr()) as GLuint;
        gl::EnableVertexArrayAttrib(vao, pos_attr);
        gl::VertexArrayAttribBinding(vao, pos_attr, 0);
        gl::VertexArrayAttribFormat(vao, pos_attr, 2, gl::FLOAT, gl::FALSE, 0);

        let col_attr = gl::GetAttribLocation(program, CString::new("color").unwrap().as_ptr()) as GLuint;
        gl::EnableVertexArrayAttrib(vao, col_attr);
        gl::VertexArrayAttribBinding(vao, col_attr, 1);
        gl::VertexArrayAttribFormat(vao, col_attr, 3, gl::FLOAT, gl::FALSE, 0);

        gl::VertexArrayVertexBuffer(vao, 0, pos_vbo, 0, (2 * mem::size_of::<GLfloat>()) as GLint);
        gl::VertexArrayVertexBuffer(vao, 1, col_vbo, 0, (3 * mem::size_of::<GLfloat>()) as GLint);
    }

    while !window.should_close() {
        glfw.poll_events();
        for (_, event) in glfw::flush_messages(&events) {
            handle_window_event(&mut window, event);
        }
        unsafe {
            gl::ClearColor(0., 0., 0., 0.);
            gl::Clear(gl::COLOR_BUFFER_BIT);

            gl::UseProgram(program);
            gl::BindVertexArray(vao);
            gl::DrawArrays(gl::TRIANGLES, 0, 3);
            gl::BindVertexArray(0);
        }
        window.swap_buffers();
    }

    unsafe {
        gl::DeleteProgram(program);
        gl::DeleteShader(fs);
        gl::DeleteShader(vs);
        gl::DeleteBuffers(1, &pos_vbo);
        gl::DeleteBuffers(1, &col_vbo);
        gl::DeleteVertexArrays(1, &vao);
    }
}

fn handle_window_event(window: &mut glfw::Window, event: glfw::WindowEvent) {
    match event {
        glfw::WindowEvent::Key(Key::Escape, _, Action::Press, _) => {
            //window.set_should_close(true)
        }
        glfw::WindowEvent::Size(w, h) => {
            println!("Resized to ({}, {})", w, h);
            unsafe {
                gl::Viewport(0, 0, w, h);
            }
        }
        _ => {}
    }
}
